\documentclass[10pt, a4paper,twocolumn]{article}

\usepackage[top=14mm, bottom=20mm, left=14mm, right=14mm]{geometry}
\usepackage{graphicx}
\usepackage{subfig}

\linespread{0.92}

\renewcommand{\abstractname}{}  

\title{
	\vspace{-4ex}
	Modelling Electron States in Silicon-Based Quantum Computers
	\vspace{-1ex}
}

\author{	
	Ryan Moodie\\
	Dr.~Brendon Lovett\\
	University of St.~Andrews
	\vspace{-1ex}
}

\date{
	August 2015\\
	\vspace{-6ex}
}

\begin{document}

	\twocolumn[
		\begin{@twocolumnfalse}
		\maketitle
		\begin{abstract}

			In experimental research towards quantum computation, planar silicon devices are made comprised of a metal gate structure over an insulating oxide layer above a silicon substrate. Voltages are applied to the gates to control the potential in the silicon and create quantum dots to confine electrons, the spin states of which encode quantum information to form qubits.

			Working with Dr.~Brendon Lovett's Theory of Quantum Nanomaterials group, this project involved theoretical work complementing these experimental developments: a simulation was produced to flexibly model such devices. Using a self-consistent method based on a Schr\"{o}dinger-Poisson solver, the wavefunctions of two electrons in quantum dots are solved for and the interaction size between them determined. Results are compared to experimental measurements and used to gauge viability of exploiting interactions between electrons in specific systems for use in quantum computation.\\

		\end{abstract}
		\end{@twocolumnfalse}
		]

	\subsection*{Original Aims}

		The project objective was to computationally model electronic states in silicon, allowing calculation of physical interaction parameters useful in the implementation of silicon-based quantum computation. Requiring microscopic system analysis, the electron wavefunctions had to be calculated before determination of coupling parameters.

		Initially, it was thought computational modelling would be best achieved using the simulation package NEMO3D. It was also considered that the exchange coupling would be the parameter focussed on as manipulation of silicon-based quantum data is generally theorised to be performed using this interaction between qubits. 

	\subsection*{Python}

		 Before the start of the project, a crude Python program which solved for the potential from a specified gate structure then solved for an electronic wavefunction in that region was shared by collaborators of the research group. Rather than NEMO3D, this formed the basis of the simulation as it provided greater flexibility in program design. It also allowed full understanding of program operation, as opposed to more `black box' packages. While increasing initial difficulty of writing the program, this choice also aided learning and allowed more low-level control. 

		 This code inspired the final simulation, with sections being used and rewritten to provide its starting point. Quantum mechanical calculations were performed using the Python package Kwant.

	\subsection*{Experimental Perspective}

		While the project concerns theoretical work, the results of the simulation can be compared to experimental measurements and used to help design systems for particular applications. To learn more about the implementation of quantum computation, the project involved a visit to University College London to meet Prof. John Morton's Quantum Spin Dynamics group. This honed simulation development to be compatible with current devices under researched and allowed a feel for which system parameters should be focussed on to generate a realistic model. The visit also confirmed the theoretical parameters the experimentalists need access to.

	\subsection*{Maturing the Simulation}

		Following this successful start, the simulation was built up to the point where the wavefunctions of two interacting electrons in a silicon substrate could be determined in two dimensions. 

		The desired gate structure is defined by creating a GDSII file\footnote{A database file format used for data exchange of integrated circuit layouts in industry.} which the simulation reads. All further settings, such as solving region boundaries, are set in a configuration file. The voltages applied to gates are set, and the potential due to this gate structure is solved for in a parallel two-dimensional electron gas at a set distance below the gate structure, this being the plane in which the rest of the simulation works. This initial potential landscape is efficiently found using an analytical solution of Poisson's equation.

		A square tight-binding lattice is set up in a region where an electron would experience confinement, generally a potential well artificially introduced by the gate structure potentials to form a quantum dot (see figure \ref{fig:potential}.). This allows the Hamiltonian to be found, and thus Schr\"{o}dinger's Equation can be solved in this region to find the wavefunction of the first electron.

		This process is repeated in a separate region in the potential landscape to yield the wavefunction of the second electron, with the difference that this solution includes the Coulomb repulsion exerted on this electron from the first.

		The simulation then returns to the first electron and solves for its wavefunction under the electrostatic interaction of the second, then repeats this for the second electron with the new electrostatic interaction of the first electron. This process forms the second step in an iterative calculation, which continues to cycle until the eigenvalues of the systems converge to a set accuracy. The results of this calculation are the self-consistent wavefunctions of the two electrons (see figure \ref{fig:wavefunctions}). 

	\subsection*{Quadrupole Interaction}

		During the course of the project, ongoing research speculated that a more robust method of qubits interaction to exploit than exchange interaction was the quadrupole interaction. A post-doc in the group, Giuseppe Pica, was involved in this research. With his collaboration, the project focus shifted from calculating the exchange coupling to investigating the quadrupole interaction, \(E_Q\), between the two electrons, given by:

		\begin{samepage}

			\[ E_Q = \frac{1}{6} \sum_{\alpha,\beta} Q_{\alpha\beta} V_{\alpha\beta}  \]

			\noindent where \(\alpha\) and \(\beta\) define system axes, \emph{V} is an externally applied electric field gradient and \emph{Q} is the quadrupole moment of the system\cite{slichter}.

		\end{samepage}

		Exchange energy is a quantum mechanical effect arising from the requirement of a two electron wavefunction to be antisymmetric under particle exchange (because electrons are fermions). As this exchange symmetry concerns the product of the spatial and spin parts of the wavefunction, different spatial configurations - with different Coulomb interactions - and spin states affect it. The exchange coupling therefore depends on the overlap of the wavefunctions of the two electrons, which are exponentially decaying in the overlap region and thus the parameter is highly sensitive to changes in potential. The quadrupole interaction, however, does not depend on wavefunction overlap. Early research indicates that it should therefore be more robust as a method of manipulating quantum information through the interaction of physical qubits. \cite{quadrupole}

		Finally, the wavefunction results are used to calculate the quadrupole moment of the system under application of an electric field gradient, allowing calculation of quadrupole interaction energy between the electrons.

		\begin{figure}

			\includegraphics[width=\linewidth]{potential}

			\caption{
				Potential landscape at a depth of 50nm from two square gates of side length 2nm set 100nm apart with applied voltages of -1V and -2V respectively. This produces two confinement regions, on which square solving regions have been centred, with the tight-binding lattices shown.
				}

			\label{fig:potential}

		\end{figure}

		\begin{figure}

			\centering

			\subfloat[Region 0]{
		        \includegraphics[width=0.45\linewidth]{probability_density_0}
				}
			\subfloat[Region 1]{
		        \includegraphics[width=0.45\linewidth]{probability_density_1}
				}

		    \caption{
			    Self-consistent probability densities of the two electrons. The simulation first solves for the wavefunction of one electron in isolation. Then it solves for the second including the Coulomb interaction from the first, and proceeds to alternate this step between the electrons until convergence in the eigenenergies is reached.
			    }

		    \label{fig:wavefunctions}

		\end{figure}

	\subsection*{Results}

		The results of simulations run at the end of the project using realistic system parameters correlate with current research into the quadrupole interaction in quantum computation, suggesting as a preliminary outcome that the quadrupole interaction is viable for use in manipulation of quantum information. This presents a positive outlook towards its future use in quantum computation.

	\subsection*{Further Research}

		With the project concluding on this promising result, the group and collaborators will continue investigation into implementation of silicon-based quantum computers and the use of quadrupole interaction in quantum computation. Moreover, the completed simulation will see more use in this research as a valuable tool for the involved scientific community.

		There is also potential for others to continue this project with extensions such as expanding the simulation to function in three dimensions. 

	\subsection*{Student Experience}
	
		My personal experience of the project has been hugely rewarding and motivating. My initial intent to follow my undergraduate Master's degree in Theoretical Physics with a PhD has blossomed from an interested but uninformed aim to a passionate and excited purpose.

		Having the opportunity to immerse myself in the research of quantum computation, and quantum mechanics and solid state physics in general, was completely captivating. Already topics of great interest to me, my knowledge, understanding and enthusiasm in them has explosively increased, which will be extremely useful for future study as these will remain core topics. Perhaps most thrillingly, I worked on and contributed to cutting-edge research in the forefront of these fields, producing academically useful and exciting results.

		I gained first-hand research experience in both theoretical and experimental environments in different leading UK universities and participated in various meetings and talks. Furthermore, I worked with some not only brilliant, but also exceedingly encouraging and friendly people, not to mention the outstanding mentorship of Dr.~Brendon Lovett. I have learnt and honed valuable skills while forging links I can return to, opening up exciting future opportunities. The project has given me a fantastic insight into postgraduate academic life and professional research, a career path I am eager to embark on. 

	\begin{thebibliography}{1}

		\bibitem{slichter}
		 	C. P. Slichter,
		 	``Electrical Quadrupole Effects'' in
		 	\emph{Principles of Magnetic Resonance},
		 	vol. 1,
		 	\emph{Springer Series in Solid-State Sciences},
		 	3rd Ed.
		 	Berlin, Germany:
		 	Springer,
		 	1989,
		 	ch. 10,
		 	sec. 2,
		 	pp. 486-489.

		\bibitem{quadrupole}
			P. A. Mortemousque et al.,
			\emph{Quadrupole Shift of Nuclear Magnetic Resonance of Donors in Silicon at Low Magnetic Field},
			\tt{arXiv:1506.04028v1 [cond-mat.mes-hall]},
			\rm{Jul 2015.}

	\end{thebibliography}

\end{document}
